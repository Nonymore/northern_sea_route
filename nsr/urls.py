# -*- coding: utf-8 -*-
from django.conf.urls import url
from nsr.views import NSRView


urlpatterns = [
    url(r'^$', NSRView.as_view(), name='nsr'),
]
