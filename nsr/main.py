# -*- coding: utf-8 -*-
from __future__ import division
from django.conf import settings
from django.core.management import call_command
from nsr_profile.models import Seaport, Vessel, WaterArea, Route, SimulationTime, Voyage, WeatherConditions, NSRZone, \
    Icebreaking, Insurance, IcebreakingQueue
from django.db.models import Q
from datetime import timedelta, datetime
from random import randint


def main(dump_name=None, first_step=None, configuration_name=None, configuration_data=None, start_date=None,
         end_date=None):
    if first_step:
        load_settings(dump_name)
    if configuration_data and configuration_name:
        generate_additional_instances(configuration_data)
        dump_data(configuration_name)
    run_simulation(start_date, end_date, configuration_data)


def load_settings(dump_name):
    call_command('flush', interactive=False)
    load_data(dump_name) if dump_name else load_data(settings['DEFAULT_DUMP'])


def load_data(dump_name):
    call_command("loaddata", dump_name)


def generate_additional_instances(data):
    if 'now' in data:
        now_data = data['now']
        for key, value in now_data.items():
            class_obj = get_class(key)
            create_new_instances(class_obj, data)
        if 'seaports' in now_data:
            activate_routes(now_data['seaports'])
        del data['now']


def get_class(class_name):
    if class_name == 'seaports':
        return Seaport
    if class_name == 'vessels':
        return Vessel
    if class_name == 'water_areas':
        return WaterArea


def create_new_instances(class_obj, data):
    for data_item in data:
        class_obj(**data_item).save()


def activate_routes(seaports):
    for seaport in seaports:
        route = Route.objects.filter(Q(source_seaport__name=seaport['name']) |
                                     Q(destination_seaport__name=seaport['name']))
        route.update(is_active=True)


def dump_data(dump_name):
    with open(dump_name, 'wb') as f:
        call_command("dumpdata", format="json", e=['auth.permission', 'contenttypes'], stdout=f)


def run_simulation(start_date, end_date, data):
    step = settings.STEP
    while start_date < end_date:
        step_weather_conditions(start_date)
        step_vessels(start_date)
        step_seaports()
        try_add_data(data)
        start_date += timedelta(hours=step)
        update_simulation_timer(step)


def step_vessels(start_date):
    process_loading_vessels(start_date)
    process_unloading_vessels(start_date)
    process_in_voyage_vessels()
    process_queue_vessels(start_date)


def process_loading_vessels(start_date):
    for vessel in Vessel.objects.filter(status='loading'):
        voyage = Voyage.objects.get(is_active=True, vessel=vessel)
        seaport = voyage.route.source_seaport
        weight_to_full = vessel.deadweight - vessel.stock
        time_to_full = weight_to_full / seaport.capacity
        if time_to_full <= 24:
            set_vessel_in_voyage(vessel, voyage, seaport, start_date, time_to_full)
        else:
            vessel.stock += 24 * seaport.capacity
            vessel.save()
            change_status(seaport, 'busy')


def set_vessel_in_voyage(vessel, voyage, seaport, start_date, time_to_full):
    change_status(vessel, 'in_voyage')
    set_traveled_distance(voyage, vessel.max_speed, 24 - time_to_full)
    set_voyage_start_date(voyage, start_date, time_to_full)
    change_status(seaport, 'open')


def change_status(obj, status):
    obj.status = status
    obj.save()


def set_traveled_distance(voyage, vessel_speed, hours):
    multiplier = get_speed_multiplier(voyage)
    change_status(voyage, vessel_speed * multiplier * hours)
    if voyage.status >= voyage.real_length:
        if voyage.route.destination_seaport.status == 'busy':
            if voyage.v_type in ['there_and_back', 'there', 'back']:
                change_status(voyage.vessel, 'queue_to_unloading')
            elif voyage.v_type == 'empty_to_back':
                change_status(voyage.vessel, 'queue_to_loading')
        else:
            if voyage.v_type in ['there_and_back', 'there', 'back']:
                change_status(voyage.vessel, 'unloading')
            elif voyage.v_type == 'empty_to_back':
                change_status(voyage.vessel, 'loading')


def get_speed_multiplier(voyage):
    return ''  # need ice_conditions


def set_voyage_start_date(voyage, start_date, time_to_full):
    voyage.start_date = start_date + timedelta(hours=time_to_full)
    voyage.save()


def process_unloading_vessels(start_date):
    for vessel in Vessel.objects.filter(status='unloading'):
        voyage = Voyage.objects.get(is_active=True, vessel=vessel)
        seaport = voyage.route.destination_seaport
        time_to_empty = vessel.stock / seaport.capacity
        if time_to_empty <= 24:
            vessel.stock = 0
            set_voyage_finish_date(voyage, start_date, time_to_empty)
            set_obj_inactive(voyage)
            new_voyage, queue = create_new_voyage(voyage)
            if queue:
                change_status(vessel, 'queue_to_icebreaking')
                vessel.stock = 0
                vessel.save()
                change_status(seaport, 'open')
                continue
            else:
                run_icebreaker(new_voyage)
            if voyage.v_type == 'there_and_back' or (voyage.v_type == 'back'
                                                     and new_voyage.v_type in ['there', 'there_and_back']):
                change_status(vessel, 'loading')
                can_stock = (24 - time_to_empty) * seaport.capacity
                time_to_full = vessel.deadweight / seaport.capacity
                if can_stock >= vessel.deadweight:
                    vessel.stock = vessel.deadweight
                    set_vessel_in_voyage(vessel, new_voyage, seaport, start_date, time_to_full + time_to_empty)
                else:
                    vessel.stock = can_stock
                    vessel.save()
            set_vessel_in_voyage(vessel, new_voyage, seaport, start_date, time_to_empty)
        else:
            vessel.stock -= seaport.capacity * 24
            vessel.save()
            change_status(seaport, 'busy')


def run_icebreaker(voyage):
    ''


def set_voyage_finish_date(voyage, start_date, time_to_empty):
    voyage.finish_date = start_date + timedelta(hours=time_to_empty)
    voyage.save()


def set_obj_inactive(obj):
    obj.is_active = False
    obj.save()


def create_new_voyage(previous_voyage):
    voyage_data, queue = get_voyage_data(previous_voyage)
    new_voyage = Voyage(**voyage_data)
    new_voyage.save()
    return new_voyage, queue


def get_voyage_data(previous_voyage):
    route = get_route(previous_voyage)
    v_type = get_voyage_type(previous_voyage.v_type)
    real_length = get_modificated_path_length(route)
    icebreaking, queue = get_icebreaking(route, previous_voyage.vessel)
    insurances = get_insurances(route, icebreaking)
    dues = get_dues(route)
    voyage_data = {'status': 0, 'route': route, 'vessel': previous_voyage.vessel, 'v_type': v_type,
                   'real_lenght': real_length, 'cargo_type': previous_voyage.cargo_type, 'icebreaking': icebreaking,
                   'insurances': insurances, 'dues': dues}
    return voyage_data, queue


def get_route(prev_voyage):
    return prev_voyage.route.invert()


def get_voyage_type(prev_type):
    if prev_type == 'there':
        return 'empty'
    elif prev_type == 'there_and_back':
        return 'back'
    elif prev_type == 'back' or 'empty':
        return ['there_and_back', 'there', 'empty_to_back'][randint(0, 2)]
    else:
        return 'back'


def get_modificated_path_length(route):
    length = route.length
    return length + randint(int(length/100), int(length/40)) + round(1/randint(1, 9), 2)


def get_icebreaking(route, vessel):
    dt = SimulationTime.objects.get(pk=1).dt
    month = dt.month
    if month in [1, 2, 3, 4, 5, 6, 11, 12]:
        navigation_period = 'winter'
    else:
        navigation_period = 'summer'
    count_of_zones = len(route.passing_zones.keys())
    gross_tonnage = vessel.gross_tonnage
    ice_class = vessel.ice_class
    price = get_icebreaking_price(count_of_zones, ice_class, navigation_period, gross_tonnage)
    icebreaker, queue = get_icebreaker()
    icebreaking = Icebreaking(price=price, icebreaker=icebreaker)
    return icebreaking, queue


def get_icebreaking_price(z_c, i_c, n_p, g_t):
    if g_t <= 5000:
        grt = 'grt < 5k'
    elif 5000 < g_t <= 10000:
        grt = '10k>grt>5k'
    elif 10000 < g_t <= 20000:
        grt = '20k>grt>10k'
    elif 20000 < g_t <= 40000:
        grt = '40k>grt>20k'
    elif 40000 < g_t <= 100000:
        grt = '100k>grt>40k'
    else:
        grt = 'grt>100k'
    return settings.ICEBREAKER_PRICE[grt][n_p][i_c][z_c]


def get_icebreaker():
    icebreakers_queryset = Vessel.objects.filter(vessel_type='icebreaker')
    icebreakers = icebreakers_queryset.filter(status='doing_nothing')
    if not icebreakers:
        icebreaker = get_icebreaker_with_min_queue(icebreakers_queryset)
        iq = IcebreakingQueue(icebreaker=icebreaker).save()
        return icebreaker, iq
    return icebreakers[0], False


def get_icebreaker_with_min_queue(icebreakers_queryset):
    s = len(icebreakers_queryset[0].queue)
    result = icebreakers_queryset[0]
    for icebreaker in icebreakers_queryset:
        if not icebreaker.queue:
            return icebreaker
        else:
            if len(icebreaker.queue) < s:
                s = len(icebreaker.queue)
                result = icebreaker
    return result


def get_insurances(route, icebreaking):
    return ''

#    passing_zones = JSONField()  # словарь айди зоны + длина маршрута по ней
#    passing_waterareas = JSONField()  # список айди акваторий


def get_dues(route):
    dues = 0
    water_areas = [WaterArea.objects.get(area_id) for area_id in route.passing_waterareas]
    for wa in water_areas:
        dues += wa.tonnage_due
        dues += wa.canal_due
        dues += wa.light_due
        dues += wa.navigation_fee
        dues += wa.pilotage_due
        dues += wa.icebreaker_due
        dues += wa.transport_safety_due
        dues += wa.ecological_due
    return dues


def step_seaports():
    for seaport in Seaport.objects.all():
        seaport.capacity += seaport.capacity_up
        seaport.save()


def step_weather_conditions(start_date):
    for zone in NSRZone.objects.all():
        weather = WeatherConditions(weather_datetime=start_date, zone=zone)
        weather.generate_weather()
        weather.generate_ice_conditions()


def try_add_data(data):
    for key in data.keys():
        key_date = datetime.strptime(key, '%Y-%m-%d')
        if key_date >= SimulationTime.objects.get(pk=1).dt:
            data['now'] = data[key]
            del data[key]
        generate_additional_instances(data)


def update_simulation_timer(step):
    sdt = SimulationTime.objects.get(pk=1)
    sdt.dt += timedelta(hours=step)


def process_queue_vessels(start_date):
    process_queue('queue_to_loading', 'source_seaport', 'loading', process_loading_vessels, start_date)
    process_queue('queue_to_unloading', 'destination_seaport', 'unloading', process_unloading_vessels, start_date)


def process_queue(query, seaport_attr, vessel_status, function, start_date):
    for vessel in Vessel.objects.filter(status=query):
        voyage = Voyage.objects.get(is_active=True, vessel=vessel)
        seaport = getattr(voyage.route, seaport_attr)
        if seaport.status == 'open':
            vessel.status = vessel_status
            vessel.save()
            function(start_date)


def process_in_voyage_vessels():
    for vessel in Vessel.objects.filter(status='in_voyage'):
        voyage = Voyage.objects.get(is_active=True, vessel=vessel)
        set_traveled_distance(voyage, vessel.max_speed, 24)


# queue_to_icebreaking
# insurance
# icebreakers
