# -*- coding: utf-8 -*-
from django.conf.urls import url
from analytic.views import AnalyticView


urlpatterns = [
    url(r'^$', AnalyticView.as_view(), name='analytic'),
]
