"""NSR URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.9/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from adminplus.sites import AdminSitePlus
from django.conf.urls import include, url
from django.contrib import admin
from django.contrib.auth import views as auth_views
from django.shortcuts import render_to_response
from django.template import RequestContext
from django.views.generic import View
from django.utils.decorators import method_decorator
from django.contrib.auth.decorators import login_required

admin.site = admin.sites.site = AdminSitePlus()
admin.autodiscover()


@method_decorator(login_required, 'dispatch')
class IndexView(View):

    def dispatch(self, *args, **kwargs):
        return super(IndexView, self).dispatch(*args, **kwargs)

    def get(self, request):
        return render_to_response('index.html', {'yolo': 'yolos'}, context_instance=RequestContext(request))


urlpatterns = [
    url(r'^profile/', include('nsr_profile.urls')),
    url(r'^nsr/', include('nsr.urls')),
    url(r'^analytic/', include('analytic.urls')),
    url(r'^$', IndexView.as_view(), name='index'),
    url(r'^login/$', auth_views.login, name='login'),
    url(r'^logout/$', auth_views.logout, name='logout'),
    url(r'^admin/', admin.site.urls),
]
