# -*- coding: utf-8 -*-
from django.conf.urls import url
from nsr_profile.vessels import VesselsList
from nsr_profile.voyages import VoyagesList
from nsr_profile.routes import RoutesList
from nsr_profile.cargos import CargosList
from nsr_profile.views import ProfileView


urlpatterns = [
    url(r'^$', ProfileView.as_view(), name='profile'),
    url(r'vessels/$', VesselsList.as_view(), name='vessels_list'),
    url(r'voyages/$', VoyagesList.as_view(), name='voyages_list'),
    url(r'routes/$', RoutesList.as_view(), name='routes_list'),
    url(r'cargos/$', CargosList.as_view(), name='cargos_list')
]
