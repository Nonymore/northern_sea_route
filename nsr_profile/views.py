# -*- coding: utf-8 -*-
from django.shortcuts import render_to_response
from django.template import RequestContext
from django.views.generic import View
from django.utils.decorators import method_decorator
from django.contrib.auth.decorators import login_required


@method_decorator(login_required, 'dispatch')
class ProfileView(View):

    def dispatch(self, *args, **kwargs):
        return super(ProfileView, self).dispatch(*args, **kwargs)

    def get(self, request):
        user = request.user
        content = {'user': user}
        return render_to_response('profile/main.html', content, context_instance=RequestContext(request))
