# -*- coding: utf-8 -*-
from django.shortcuts import render_to_response
from django.template import RequestContext
from django.views.generic import View


class CargosList(View):

    def dispatch(self, *args, **kwargs):
        return super(CargosList, self).dispatch(*args, **kwargs)

    def get(self, request):
        content = {'content': 'content'}
        return render_to_response('profile/cargos.html', content, context_instance=RequestContext(request))
