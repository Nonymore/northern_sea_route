# -*- coding: utf-8 -*-
from models import RouteVerge, Route, WeatherConditions
import json
from django.shortcuts import render_to_response
from django.template import RequestContext
from django.views.generic import View


def get_routes(vessel, start_node, finish_node, direction, start_date):
    possible_routes = get_possible_routes(start_node, finish_node, direction)
    possible_routes = [Route.objects.get_or_create(route=json.dumps(possible_route)) for possible_route in
                       possible_routes]
    routes = filter_routes_by_vessel(vessel, possible_routes, start_date)


# def get_first_possible_date_for_voyage(vessel, possible_routes):


def get_possible_routes(start_node, finish_node, direction):
    possible_routes = []
    i = 0
    verges = RouteVerge.objects.filter(west_point=start_node) if direction == 'East' \
        else RouteVerge.objects.filter(east_point=start_node)
    for verge in verges:
        possible_routes.append({1: verge.east_point.id + verge.west_point.id})
        get_children_routes(verge, direction, possible_routes, i, finish_node)
        i += 1
    return possible_routes


def get_children_routes(verge, direction, possible_routes, i, finish_node):
    verges = RouteVerge.objects.filter(west_point=verge.east_point) if direction == 'East' \
        else RouteVerge.objects.filter(east_point=verge.west_point)
    if verges.count > 1:
        [possible_routes.append(possible_routes[i].copy()) for useless_counter in xrange(verges.count() - 1)]
    for verge in verges:
        possible_routes[i].update({max(possible_routes[i].keys()) + 1: verge})
        if verge.east_point == finish_node or verge.west_point == finish_node:
            i += 1
            continue
        get_children_routes(verge, direction, possible_routes, i, finish_node)
        i += 1


def filter_routes_by_vessel(vessel, possible_routes, start_date):
    routes_zones, routes_water_areas = get_verge_zones_and_water_areas(possible_routes)
    ice_conditions = get_ice_conditions_for_possible_routes(routes_zones, start_date)
    return get_real_routes_for_vessel(vessel, possible_routes, ice_conditions)


def get_verge_zones_and_water_areas(possible_routes):
    routes_zones = {}
    routes_water_areas = {}
    zones = []
    water_areas = []
    for possible_route in possible_routes:
        for verge in possible_route.values():
            water_area = verge.water_area
            if water_area and water_area not in water_areas:
                water_areas.append(water_area)
            east_point_nsr_zone = verge.east_point.nsr_zone
            west_point_nsr_zone = verge.west_point.nsr_zone
            if east_point_nsr_zone not in zones:
                zones.append(east_point_nsr_zone)
            if west_point_nsr_zone not in zones:
                zones.append(west_point_nsr_zone)
        routes_zones.update({possible_route: zones})
        routes_water_areas.update({possible_route: water_areas})
    return routes_zones, routes_water_areas


def get_ice_conditions_for_possible_routes(routes_zones, start_date):
    ice_conditions = {}
    weather_conditions = []
    #  for route_zone in routes_zones:

    weather_conditions = [WeatherConditions.objects.get()]

    return [ice_conditions.update({route: {zone.id: zone.weather_conditions for zone in zones}})
            for route, zones in routes_zones.items()]


def get_real_routes_for_vessel(vessel, possible_routes, ice_conditions):
    real_routes = []

# get_routes('vessel', RouteNode.objects.get(id='a'), RouteNode.objects.get(id='f'), 'West')


class RoutesList(View):

    def dispatch(self, *args, **kwargs):
        return super(RoutesList, self).dispatch(*args, **kwargs)

    def get(self, request):
        content = {'content': 'content'}
        return render_to_response('profile/routes.html', content, context_instance=RequestContext(request))
