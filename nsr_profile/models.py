# -*- coding: utf-8 -*-
from __future__ import division
from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.contrib.postgres.fields import JSONField, ArrayField
from django.contrib.auth.models import User
from django.conf import settings
from random import randint


class SimulationTime(models.Model):
    dt = models.DateTimeField()


class NSRZone(models.Model):  # зона СМП
    name = models.CharField(max_length=255)  # наименование
    additional_risks = models.FloatField(null=True)  # дополнительные риски (коэфф.) - вычисляемое

    def get_risks(self):
        weather = self.weather
        risk = (weather.fog + weather.polar_night + weather.squally_wind + weather.bitter_cold +
                weather.drifting_ice) * 5
        risk += weather.ice_conditions * 10
        return risk/100


class WeatherConditions(models.Model):
    ICE_CONDITIONS = ((1, _('easy')), (2, _('medium')), (3, _('hard')))  # тяжесть ледовых условий
    fog = models.BooleanField(default=False)  # наличие тумана
    polar_night = models.BooleanField(default=False)  # полярная ночь со 2 декабря по 10 января
    squally_wind = models.BooleanField(default=False)  # шквалистый ветер
    bitter_cold = models.BooleanField(default=False)  # сильный мороз
    drifting_ice = models.BooleanField(default=False)  # дрейфующие льды
    weather_datetime = models.DateTimeField()  # дата и время
    ice_conditions = models.IntegerField(choices=ICE_CONDITIONS)  # ледовые условия
    zone = models.OneToOneField(NSRZone, related_name='weather', null=True)

    class Meta:
        unique_together = (('weather_datetime', 'zone'),)

    def generate_weather(self):
        for field in self._meta.fields:
            if type(field) == models.fields.BooleanField:
                if randint(1, 10) > 1:
                    setattr(self, field.name, False)
                else:
                    setattr(self, field.name, True)
        month = self.weather_datetime.month
        day = self.weather_datetime.day
        if (month == 12 and day >= 2) or (day <= 10 and month == 1):
            self.polar_night = True
        else:
            self.polar_night = False
        self.save()

    def generate_ice_conditions(self):
        month = self.weather_datetime.month
        if self.zone.name in ['SWK', 'NEK']:
            if month in [7, 8, 9, 10]:
                self.ice_conditions = 1
            elif month in [11, 12, 5, 6]:
                self.ice_conditions = 1
            else:
                self.ice_conditions = 2
        elif self.zone.name in ['WLA', 'ELA']:
            if month in [7, 8, 9, 10]:
                self.ice_conditions = 1
            elif month in [11, 12, 5, 6]:
                self.ice_conditions = 2
            else:
                self.ice_conditions = 2
        else:
            if month in [7, 8, 9, 10]:
                self.ice_conditions = 1
            elif month in [12, 6]:
                self.ice_conditions = 2
            else:
                self.ice_conditions = 3
        self.save()


class Vessel(models.Model):  # судно, в т.ч. ледокол
    ICE_CLASS = ((None, _('None')), ('ice1', 'Ice1'), ('ice2', 'Ice2'), ('arc4', 'Arc4'), ('arc7', 'Arc7'),
                 ('Icebreaker', 'IceBreaker'))
    VESSEL_TYPE = (('icebreaker', _('Icebreaker')), ('cargo_vessel', _('Cargo vessel')))
    STATUS = (('loading', _('loading')), ('unloading', _('unloading')),
              ('queue_to_unloading', _('queue_to_unloading')), ('queue_to_loading', _('queue_to_loading')),
              ('queue_to_icebreaking', _('queue_to_icebreaking')), ('in_voyage', _('in_voyage')),
              ('doing_nothing', _('doing_nothing')))
    name = models.CharField(max_length=255)  # наименование
    gross_tonnage = models.FloatField()  # валовая вместимость
    deadweight = models.FloatField()  # дедвейт - максимум груза на борту в тыс. тонн
    stock = models.FloatField()  # к-во груза в тоннах
    ice_class = models.CharField(null=True, max_length=255, choices=ICE_CLASS)  # ледовый класс
    max_speed = models.FloatField()  # максимальная скорость хода
    vessel_type = models.CharField(max_length=255, choices=VESSEL_TYPE)  # тип
    status = models.CharField(choices=STATUS)


class Seaport(models.Model):
    STATUS = (('busy', _('busy')), ('open', _('open')))
    name = models.CharField(max_length=255)  # наименование
    capacity = models.FloatField(null=True)  # портовые мощности, здесь - скорость погрузки/разгрузки судов млн.т./год
    status = models.CharField(max_length=255, choices=STATUS)
    capacity_up = models.FloatField(null=True)  # рост мощности в день


class Route(models.Model):  # маршрут
    source_seaport = models.ForeignKey(Seaport)
    destination_seaport = models.ForeignKey(Seaport)
    length = models.FloatField()
    passing_zones = JSONField()  # словарь айди зоны + длина маршрута по ней
    passing_waterareas = JSONField()  # словарь айди акватории + тип прохождения (транзит/заход/выход)
    is_active = models.BooleanField(default=True)

    def invert(self):
        return Route(source_seaport=self.destination_seaport, destination_seaport=self.source_seaport)


class Insurance(models.Model):  # страховка
    TYPE = (('H&M', _('H&M')), ('ML', _('ML')))  # H&M - Hull and Machinery (КММ),
    # ML - Marine Liability (Ответственность судовладельца перед 3ми лицами)
    premium = models.FloatField(null=True, default=None)  # страховая премия
    type = models.CharField(max_length=255, choices=TYPE)  # тип страхования
    amount = models.FloatField(null=True, default=None)  # страховая сумма


class Icebreaking(models.Model):  # ледокольная проводка
    icebreaker = models.ForeignKey(Vessel)  # ледокол
    price = models.FloatField(null=True)  # стоимость проводки


class Voyage(models.Model):
    CARGO_TYPE = (('non-dangerous', _('non-dangerous')), ('dangerous', _('dangerous')))  # типы грузов (по опасности)
    V_TYPE = (('there_and_back', _('there_and_back')), ('there', _('there')), ('back', _('back')),
              ('empty', _('empty')), ('empty_to_back', _('empty_to_back')))
    NAVIGATION_PERIOD = (('winter', _('winter')), ('summer', _('summer')))
    navigation_period = models.CharField(max_length=255, choices=NAVIGATION_PERIOD)
    status = models.FloatField(default=1)  # текущий км по маршруту
    real_length = models.FloatField(default=None, null=True)  # длина маршрута с учетом отклонений
    route = models.ForeignKey(Route)  # маршрут
    vessel = models.ForeignKey(Vessel)  # судно
    icebreaking = models.ForeignKey(Icebreaking, null=True, blank=True)  # ледокольная проводка
    insurances = ArrayField(base_field=models.IntegerField(null=True))  # список страховок
    dues = models.FloatField(null=True)  # сумма сборов
    start_date = models.DateTimeField(null=True, blank=True)  # фактическая дата начала плавания
    finish_date = models.DateTimeField(null=True, blank=True)  # фактическая дата завершения плавания
    cargo_type = models.CharField(max_length=255, choices=CARGO_TYPE)  # тип груза, важно для страхования
    is_active = models.BooleanField(default=True)  # активное/архивное плавание
    v_type = models.CharField(max_length=255, choices=V_TYPE)

    def __init__(self, *args, **kwargs):
        dt = SimulationTime.objects.get(pk=1).dt
        month = dt.month
        if month in [1, 2, 3, 4, 5, 6, 11, 12]:
            self.navigation_period = 'winter'
        else:
            self.navigation_period = 'summer'
        super(Voyage, self).__init__(*args, **kwargs)


class WaterArea(models.Model):  # акватория порта
    seaport = models.ForeignKey(Seaport)  # наименование
    tonnage_due = models.FloatField(null=True, default=settings.DUE['tonnage'])  # корабельный сбор
    canal_due = models.FloatField(null=True, default=settings.DUE['canal'])  # канальный сбор
    light_due = models.FloatField(null=True, default=settings.DUE['light'])  # маячный сбор
    navigation_fee = models.FloatField(null=True, default=settings.DUE['navigation'])  # навигационный сбор
    pilotage_due = models.FloatField(null=True, default=settings.DUE['pilotage'])  # лоцманский сбор
    icebreaker_due = models.FloatField(null=True, default=settings.DUE['icebreaker'])  # ледокольный сбор
    transport_safety_due = models.FloatField(null=True, default=settings.DUE['transport_safety'])  # сбор
    # транспортной безопасности акватории морского порта
    ecological_due = models.FloatField(null=True, default=settings.DUE['ecological'])  # экологический сбор


class IcebreakingQueue(models.Model):
    icebreaker = models.ForeignKey(Icebreaking, related_name='queue')
    is_completed = models.BooleanField(default=False)
