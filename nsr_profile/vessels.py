# -*- coding: utf-8 -*-
from django.shortcuts import redirect
from django.http import HttpResponse, HttpResponseBadRequest
from django.shortcuts import render_to_response, get_object_or_404
from django.template import RequestContext
import json
from django.views.decorators.csrf import csrf_exempt
from django.db.models import Count, Q, F
from django.contrib.auth.decorators import permission_required, login_required
from django.utils.encoding import iri_to_uri
from django.views.generic import View
from django.contrib.auth import get_user_model
from django.contrib.auth.models import Permission
from datetime import datetime
from django.utils.decorators import method_decorator
from django.contrib import messages
from django.template.context_processors import csrf
import xlrd
import re
from django.core.validators import URLValidator
from django.core.exceptions import ValidationError, ObjectDoesNotExist
from django.db.utils import IntegrityError
import logging
from rest_framework.response import Response
from rest_framework import status
from rest_framework.decorators import api_view
from django.utils.decorators import method_decorator


@method_decorator(login_required, 'dispatch')
class VesselsList(View):

    def dispatch(self, *args, **kwargs):
        return super(VesselsList, self).dispatch(*args, **kwargs)

    def get(self, request):
        user = request.user
        content = {'vessels': [vessel for vessel in user.vessels.all()]}
        return render_to_response('profile/vessels.html', content, context_instance=RequestContext(request))


#     def post(self, request, id):
#         template = get_object_or_404(RuleTemplate, pk=id)
#         rules = Rule.objects.filter(template=template)
#         if request.POST.get('type') == 'remove':
#             if not rules:
#                 template.delete()
#                 return HttpResponse('Успешно удалено')
#             else:
#                 return HttpResponse('Удалить невозможно, существуют правила')
#         return HttpResponse('Неверный запрос')
#
# @permission_required('parsiso.parsiso_show')
# def edit_report_step2_json(request, id, date=None, types='html'):
#     if request.GET.get('date1'):
#         date = request.GET.get('date1')
#
#     if date:
#         iteration = get_object_or_404(Iteration, created=date)
#     else:
#         iteration = Iteration.objects.all()[0]
#
#     report = get_object_or_404(Report, pk=id)
#     result = ReportBuilder().build(report, iteration)
#
#     return HttpResponse(json.dumps(result), content_type="application/json", status=200)
